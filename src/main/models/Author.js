module.exports = function (sequelize, DataTypes) {
  return sequelize.define('author', {
    name: {
      type: DataTypes.STRING,
      unique: true
    }
  }, {
    limit: 1000
  })
}
