import Logger from './Logger'
import Showdown from 'showdown'
import Turndown from 'turndown'

Logger.info('Instantiating Markdown service.')

/**
 * Kids, save yourself some time by NOT trying to assign functions to new
 * variable names. It's (THIS IS) a bloody recipe for disaster.
 */
const HTMLService = new Showdown.Converter()
const MarkdownService = new Turndown()

const toHtml = function (string) {
  return HTMLService.makeHtml(string)
}
const toMarkdown = function (string) {
  return MarkdownService.turndown(string)
}

global.MarkdownService = {
  toHtml, toMarkdown
}

export {
  toHtml, toMarkdown
}
