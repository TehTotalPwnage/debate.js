import axios from 'axios'
import { load } from 'cheerio'
import EventEmitter from 'events'
import logger from './Logger'
import { toMarkdown } from './MarkdownService'
import OverrideError from './OverrideError'

/**
 * Base interface for news article readers.
 *
 * To implement a new reader, please override all of the abstract getters and
 * functions, taking care to keep any keywords such as static or async.
 * @interface
 */
class BaseReader extends EventEmitter {
  /**
   * @param {string} url The URL that the reader should parse.
   */
  constructor (url) {
    super()
    this.url = url || this.constructor._homepage
    logger.debug('Instantiated reader for handling ' + this.constructor.source + '.')
    logger.debug('Reader will pull from: ' + this.url)
  }

  // The following three methods are used to define general source information.
  /**
   * The URL basename of the news source. Used for URL checking.
   * @abstract
   * @type {string}
   */
  static get basename () {
    throw new OverrideError()
  }

  /**
   * The homepage URL for the news source. Used for pulling.
   * @abstract
   * @type {string}
   */
  static get _homepage () {
    throw new OverrideError()
  }

  /**
   * The name of the news source.
   * @abstract
   * @type {string}
   */
  static get source () {
    throw new OverrideError()
  }

  get isIndividual () {
    return this.url !== this.constructor._homepage
  }

  // The following method is for article pulling.
  /**
   * A list of article URLs from the homepage.
   * @abstract
   * @type {string[]}
   */
  _articles ($) {
    throw new OverrideError()
  }

  // The following remaining properties are for saving individual articles.
  /**
   * The list of authors who wrote the article.
   * @abstract
   * @type {string[]}
   */
  _authors ($) {
    return []
  }

  /**
   * The HTML body of the article.
   * @abstract
   * @type {string}
   */
  _content ($) {
    throw new OverrideError()
  }

  /**
   * The last modification date of the article.
   * @abstract
   * @type {Date}
   */
  _modified ($) {
    return null
  }

  /**
   * The original publication date of the article.
   * @abstract
   * @type {Date}
   */
  _published ($) {
    return null
  }

  /**
   * An array of predefined tags on the article.
   * @abstract
   * @type {string[]}
   */
  _tags ($) {
    return []
  }

  /**
   * The title of the article.
   * @abstract
   * @type {string}
   */
  _title ($) {
    throw new OverrideError()
  }

  // static async getCss($) {
  //     let rules = [];
  //     for (let style of $('style').toArray()) {
  //         rules.push(style.firstChild);
  //     }
  //     for (let link of $('link[rel=stylesheet]').toArray()) {
  //         let style = await rp({
  //             uri: link.attribs.href
  //         });
  //         rules.push(style);
  //     }
  //     return rules;
  // }

  /* *
   * The following methods dictate the behavior of the actual download tasks.
   * By default, these methods should not be overriden.
   */

  /**
   * Downloads the article represented by this reader and returns an object
   * holding its contents.
   *
   * @returns {Object} A Object representing a news article.
   */
  async _download () {
    return axios.get(this.url, {
      onDownloadProgress: progressEvent => {
        if (progressEvent.lengthComputable) {
          this.emit('progress', progressEvent)
        }
      },
      transformResponse: [function (data) {
        return load(data)
      }]
    }).then(data => {
      this.removeAllListeners()
      return data
    })
  }

  /**
   * Parses the article currently represented by the instance and returns the
   * article's contents.
   *
   * If the current instance instead represents a pulling task, this function
   * will throw an error.
   */
  async parse () {
    if (this.isIndividual) {
      logger.info(`Using ${this.constructor.source}'s reader to parse ${this.url}.`)
      let response = await this._download(this.url)
      let $ = response.data

      return {
        title: this._title($),
        authors: this._authors($),
        tags: this._tags($),
        content: toMarkdown(this._content($)),
        published: this._published($),
        modified: this._modified($),
        source: this.constructor.source,
        url: this.url
      }
    } else {
      throw new Error('parse() cannot be called on an individual instance')
    }
  }

  /**
   * Loads the front page links for the source represented by this reader and
   * returns them in an array format.
   *
   * If the current instance instead represents an individual article, this
   * function will throw an error.
   */
  async pull () {
    if (!this.isIndividual) {
      let response = await this._download(this.url)
      let $ = response.data
      return this._articles($)
    } else {
      throw new Error('pull() cannot be called on a pulling instance')
    }
  }
}

export default BaseReader
