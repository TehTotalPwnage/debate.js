import { app } from 'electron'
import { resolve } from 'path'
import Sequelize from 'sequelize'
import logger from './Logger'

import Article from '../models/Article'
import Author from '../models/Author'
import Tag from '../models/Tag'

const dateOnly = function (date) {
  return {
    [lt]: new Date(new Date(date) + 24 * 60 * 60 * 1000),
    [gt]: new Date(new Date(date) - 24 * 60 * 60 * 1000)
  }
}
const dbpath = resolve(app.getPath('userData'), 'database.sqlite')
const { gt, lt, ne } = Sequelize.Op
const notNull = {
  [ne]: null
}

/**
 * Useful wrapping class for Sequelize.
 * And by useful I mean Electron is so retarded when it comes to handling contexts.
 */
class Database {
  /**
   * HAHA ELECTRON WON'T BLOODY LET ME EXPOSE THE MODELS INDEPENDENTLY.
   * HAHA ELECTRON KEEPS TURNING THEM INTO ANONYMOUS GETTERS.
   * HAHA SCREW ELECTRON THIS IS TAKING TOO LONG TO IMPLEMENT A SIMPLE SEARCH.
   */
  async initialize () {
    this.sequelize = global.sequelize = new Sequelize('database', 'username', null, {
      dialect: 'sqlite',
      storage: dbpath
    })
    logger.info('Instantiated Sequelize instance at ' + dbpath + '.')
    await this._initializeModels()
    await this._initializeAssociations()
    await this.sequelize.sync()
    this.initialized = true
    logger.info('Database instance ready. Exposing to globals.')
    global.Database = this
  }

  async _initializeAssociations () {
    let ArticleAuthor = this.sequelize.define('article_author', {})
    this._article.belongsToMany(this._author, {
      through: ArticleAuthor
    })
    this._author.belongsToMany(this._article, {
      through: ArticleAuthor
    })
    logger.debug('Declaring BelongsToMany relationship between Article and Author.')

    let ArticleTag = this.sequelize.define('article_tag', {})
    this._article.belongsToMany(this._tag, {
      through: ArticleTag
    })
    this._tag.belongsToMany(this._article, {
      through: ArticleTag
    })
    logger.debug('Declaring BelongsToMany association between Article and Tag.')
  }

  async _initializeModels () {
    this._article = this.sequelize.import('article', Article)
    logger.debug('Defined Article model.')
    this._author = this.sequelize.import('author', Author)
    logger.debug('Defined Author model.')
    this._tag = this.sequelize.import('tag', Tag)
    logger.debug('Defined Tag model.')
  }

  async all () {
    return this._article.findAll()
  }

  async find (id) {
    return this._article.findById(id, {
      include: [
        {
          model: this._author,
          attributes: ['name']
        },
        {
          model: this._tag,
          attributes: ['name']
        }
      ]
    })
  }

  async get (model, term) {
    let obj = (model === 'author' ? this._author : this._tag)
    return obj.findAll({
      where: {
        name: {
          $like: '%' + term + '%'
        }
      },
      limit: 10
    })
  }

  async search (params) {
    return this._article.findAll({
      include: [
        {
          model: this._author,
          where: {
            name: params.authors
          },
          required: params.authors.length !== 0
        },
        {
          model: this._tag,
          where: {
            name: params.tags
          },
          required: params.tags.length !== 0
        }
      ],
      where: {
        title: {
          $like: '%' + params.title + '%'
        },
        published: params.published ? dateOnly(params.published) : notNull,
        modified: params.modified ? dateOnly(params.modified) : notNull,
        source: params.source || notNull,
        url: params.url || notNull
      }
    })
  }
}

const instance = new Database()

export default instance
