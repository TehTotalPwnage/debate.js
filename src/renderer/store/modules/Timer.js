const state = {
  current: null,
  initial: null,
  interval: null,
  startTime: null,
  state: 0,
  timeout: null,
  times: {
    extemp: 1000 * 60 * 30
  }
}

const getters = {
  current: state => {
    return state.current
  },
  extemp: state => {
    return state.times.extemp
  },
  state: state => {
    return state.state
  }
}

const mutations = {
  endTimer (state) {
    alert('Prep time has ended!')
  },
  // The Date object isn't reactive and would not work when computing the current time.
  // Instead, we calculate the time in a method and manually call it using setInterval.
  // https://vuejs.org/v2/guide/computed.html#Computed-Caching-vs-Methods
  getTime (state) {
    console.log('Updating time.')
    state.current = state.initial - ((new Date()).getTime() - state.startTime)
  },
  pause (state) {
    clearTimeout(state.timeout)
    state.timeout = null
    clearInterval(state.interval)
    state.interval = null
    state.state = 2
  },
  start (state, payload) {
    console.debug('Timer starting.')
    state.initial = state.current || state.times.extemp
    state.timeout = setTimeout(payload.timer, state.initial)
    state.startTime = (new Date()).getTime()
    state.interval = setInterval(payload.interval, 1000)
    state.state = 1
  },
  stop (state) {
    clearTimeout(state.timeout)
    state.current = null
    state.timeout = null
    clearInterval(state.interval)
    state.interval = null
    state.state = 0
  }
}

const actions = {
  toggle (context) {
    context.state.state === 1 ? context.commit('pause') : context.commit('start', {
      interval: () => context.commit('getTime'),
      timer: () => {
        context.commit('endTimer')
        context.commit('stop')
      }
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
