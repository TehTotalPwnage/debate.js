import App from '@/App'
import ArticleGenerator from '@/../main/tasks/ArticleGenerator'
// Love the work you're doing ESLint, but I REALLY REALLY need this import
// for my unit tests. Thanks. <3
// eslint-disable-next-line
import Injector from 'inject-loader!@/store/modules/Puller'
// Reminder, you CANNOT destructure ES2015 imports.
import Puller from '@/store/modules/Puller'

import sinon from 'sinon'
import { shallow, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'

const { getters, mutations } = Puller

const localVue = createLocalVue()
localVue.use(VueRouter)
const router = new VueRouter({routes: [
  {
    path: '/',
    name: 'landing-page',
    component: require('@/components/LandingPage').default
  },
  {
    path: '/index',
    name: 'index',
    component: require('@/components/Index').default
  },
  {
    path: '/articles/create',
    name: 'articles.create',
    component: require('@/components/article/Create').default
  }
]})

const wrapper = shallow(App, {
  localVue,
  router
})

const actions = Injector({
  '@/router/index': {
    // Remember to reroute functions in this manner. It's longer but guarantees
    // that the "this" context isn't absolutely screwed over.
    push: function (args) {
      wrapper.vm.$router.push(args)
    }
  }
}).default.actions

describe('Puller store getters', () => {
  // This should be tested in case the Generator name varies by environment.
  it('should detect the article generator', () => {
    const state = { articles: ArticleGenerator() }
    // eslint-disable-next-line no-unused-expressions
    expect(getters.active(state)).to.be.true
    state.articles = {}
    // eslint-disable-next-line no-unused-expressions
    expect(getters.active(state)).to.be.false
  })

  // The puller getter is simple enough to not require a test.
})

describe('Puller store mutations', () => {
  it('should properly set the article generator', () => {
    const state = { articles: {} }
    mutations.set(state, ArticleGenerator())
    // eslint-disable-next-line no-unused-expressions
    expect(getters.active(state)).to.be.true
  })
})

describe('Puller store actions', () => {
  it('should properly cancel', () => {
    // There's a block of code meant to test which commits are being fired available
    // at https://vuex.vuejs.org/en/testing.html. However, it's a PURE MESS to the
    // point that Junko would be jealous. As a result, I'm just going to grow my
    // ever-expanding black hole of the node_modules folder and install Sinon.JS.
    const commit = sinon.spy()
    const state = {}

    actions.cancel({ commit, state })

    expect(commit.args).to.deep.equal([
      ['set', {}]
    ])

    expect(wrapper.vm.$route.name).to.equal('index')
  })

  it('should parse the next article in a generator', () => {
    const commit = sinon.spy()

    // Instead of using ArticleGenerator we're going to use our own stubbed generator
    // due to the complexity of the other generator that should only be touched in
    // end to end testing.
    const generator = (function* () {
      for (let url of ['https://ttp.codes', 'https://www.ttp.codes']) {
        yield url
      }
    })()
    generator.next()
    const state = {
      articles: generator
    }

    actions.next({
      commit,
      getters: {
        get puller () {
          return state.articles
        }
      },
      state
    })

    expect(commit.args).to.deep.equal([
      ['set', generator]
    ])

    expect(wrapper.vm.$route.name).to.equal('articles.create')
    expect(wrapper.vm.$route.query.link).to.equal('https://www.ttp.codes')
  })

  it('should properly handle the end of a generator', () => {
    const commit = sinon.spy()

    // Instead of using ArticleGenerator we're going to use our own stubbed generator
    // due to the complexity of the other generator that should only be touched in
    // end to end testing.
    const generator = (function* () {
      for (let url of ['https://ttp.codes', 'https://www.ttp.codes']) {
        yield url
      }
    })()
    generator.next()
    generator.next()
    const state = {
      articles: generator
    }

    actions.next({
      commit,
      getters: {
        get puller () {
          return state.articles
        }
      },
      state
    })

    expect(commit.args).to.deep.equal([
      ['set', {}]
    ])
    expect(wrapper.vm.$route.name).to.equal('index')
    // Okay, using jQuery here is really messy BUT the Vue method can't seem to
    // find the aforementioned notification so... yeah, we're keeping this.
    expect($('[data-notify=message]').text()).to
      .contain('Pulling class parsed all articles.')
  })

  it('should properly handle a new generator', () => {
    const commit = sinon.spy()

    // Instead of using ArticleGenerator we're going to use our own stubbed generator
    // due to the complexity of the other generator that should only be touched in
    // end to end testing.
    const generator = (function* () {
      for (let url of ['https://ttp.codes', 'https://www.ttp.codes']) {
        yield url
      }
    })()
    generator.next()
    const state = {
      articles: generator
    }

    actions.next({
      commit,
      getters: {
        get puller () {
          return state.articles
        }
      },
      state
    })

    expect(commit.args).to.deep.equal([
      ['set', generator]
    ])

    expect(wrapper.vm.$route.name).to.equal('articles.create')
    expect(wrapper.vm.$route.query.link).to.equal('https://www.ttp.codes')
  })

  it('should properly handle the end of a generator', () => {
    const commit = sinon.spy()

    // Instead of using ArticleGenerator we're going to use our own stubbed generator
    // due to the complexity of the other generator that should only be touched in
    // end to end testing.
    const generator = (function* () {
      for (let url of ['https://ttp.codes', 'https://www.ttp.codes']) {
        yield url
      }
    })()
    const state = {}

    actions.set({ commit, state }, generator)

    expect(commit.args).to.deep.equal([
      ['set', generator]
    ])
    expect(wrapper.vm.$route.name).to.equal('articles.create')
    expect(wrapper.vm.$route.query.link).to.equal('https://ttp.codes')
  })
})
