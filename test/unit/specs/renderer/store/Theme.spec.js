import sinon from 'sinon'
import Theme from '@/store/modules/Theme'

const { actions, getters, mutations } = Theme

describe('Theme store mutations', () => {
  it('should properly set the theme to dark', () => {
    const state = { theme: 0 }
    mutations.dark(state)

    expect(getters.theme(state)).to.equal(1)
    expect($('body').css('backgroundColor')).to.equal('rgb(34, 34, 34)')
  })

  it('should properly set the theme to light', () => {
    const state = { theme: 1 }
    mutations.light(state)

    expect(getters.theme(state)).to.equal(0)
    expect($('body').css('backgroundColor')).to.equal('rgb(255, 255, 255)')
  })
})

describe('Theme store actions', () => {
  it('should properly toggle the theme based on current state', () => {
    const commit = sinon.spy()
    const state = { theme: 0 }

    actions.theme({ commit, state })

    state.theme = 1
    actions.theme({ commit, state })

    expect(commit.args).to.deep.equal([
      ['dark'],
      ['light']
    ])
  })
})
